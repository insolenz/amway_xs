﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Website.Models;

namespace Website.Areas.Admin.Filters
{
    public class AdminAuthorizeFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Request.IsLocal)
            {
                WebContext.AdminUserID = 1;
                WebContext.AdminUserLevel = 1;
            }
            else
            {
                if (!WebContext.IsAdmin) HttpContext.Current.Response.Redirect("~/backend/adminuser/login?next=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri));
            }

            base.OnActionExecuting(filterContext);
        }
    }

    /*
    public class SystemAdminAuthorizeFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (WebContext.AdminUserLevel == null || WebContext.AdminUserLevel > (int)AdminLevelEnum.SystemAdmin) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminAdministratorRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();

            if (WebContext.AdminUserLevel == null || WebContext.AdminUserLevel > (int)AdminLevelEnum.SystemAdmin)
            {
                AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Administrator).FirstOrDefault();

                if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");
            }

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminBrnchRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Brnch).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminBannerRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Banner).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminHappyLifeClubRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.HappyLifeClub).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminPrivilegeRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Privilege).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminPromotionRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Promotion).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminActivityRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Activity).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminNewsRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.News).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }

    public class AdminEmagazineRole : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DBEntities db = new DBEntities();
            AdminInRole rolse = db.AdminInRole.Where(q => q.IsDelete == false && q.AdminID == WebContext.AdminUserID.Value && q.RoleID == (long)AdminRoleEnum.Emagazine).FirstOrDefault();

            if (rolse == null) HttpContext.Current.Response.Redirect("~/admin");

            base.OnActionExecuting(filterContext);
        }
    }
    */

}