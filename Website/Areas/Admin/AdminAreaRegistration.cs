﻿using System.Web.Mvc;

namespace Website.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            //context.MapRoute(
            //  "Admin_MagazineGallery",
            //  "Backend/MagazineGallery/{action}/{pid}/{id}",
            //  new { controller = "MagazineGallery", action = "Index", id = UrlParameter.Optional }
            //);

            //context.MapRoute(
            //  "Admin_Magazine",
            //  "Backend/Magazine/{action}/{tid}/{id}",
            //  new { controller = "Magazine", action = "Index", id = UrlParameter.Optional }
            //);

            //context.MapRoute(
            //  "Admin_NewsGallery",
            //  "Backend/NewsGallery/{action}/{pid}/{id}",
            //  new { controller = "NewsGallery", action = "Index", id = UrlParameter.Optional }
            //);

            //// Privilege
            //context.MapRoute(
            //   "Admin_PrivilegeGallery",
            //   "Backend/PrivilegeGallery/{action}/{pid}/{id}",
            //   new { controller = "PrivilegeGallery", action = "Index", id = UrlParameter.Optional }
            //);

            //context.MapRoute(
            //    "Admin_Privilege",
            //    "Backend/Privilege/{action}/{cid}/{id}",
            //    new { controller = "Privilege", action = "Index", id = UrlParameter.Optional }
            //);

            //// Promotion
            //context.MapRoute(
            //   "Admin_PromotionGallery",
            //   "Backend/PromotionGallery/{action}/{pid}/{id}",
            //   new { controller = "PromotionGallery", action = "Index", id = UrlParameter.Optional }
            //);

            //context.MapRoute(
            //    "Admin_Promotion",
            //    "Backend/Promotion/{action}/{cid}/{id}",
            //    new { controller = "Promotion", action = "Index", id = UrlParameter.Optional }
            //);

            context.MapRoute(
                "Admin_default",
                "Backend/{controller}/{action}/{id}",
                new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional },
                new string[] { "Website.Areas.Admin.Controllers" }
            );
        }
    }
}
