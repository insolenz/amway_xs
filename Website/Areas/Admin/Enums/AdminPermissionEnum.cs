﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Areas.Admin
{
    public enum AdminPermissionEnum
    {
        CreateList = 1,
        EditList = 2,
        DeleteList = 3,
        ReadOnly = 4
    }
}