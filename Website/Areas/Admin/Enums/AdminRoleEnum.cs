﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Areas.Admin
{
    public enum AdminRoleEnum
    {
        Administrator = 1,
        Brnch = 2,
        Banner = 3,
        HappyLifeClub = 4,
        Privilege = 5,
        Promotion = 6,
        Activity = 7,
        News = 8,
        Emagazine = 9,
    }
}