﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Areas.Admin
{
    public enum AdminLevelEnum
    {
        SystemAdmin = 1,
        Admin = 2,
        User = 3
    }
}