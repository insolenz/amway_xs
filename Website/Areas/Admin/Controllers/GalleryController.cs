﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website;

using Website.Models;
using Website.Areas.Admin.Filters;
using Website.Areas.Admin.Models;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Website.Areas.Admin.Controllers
{
    [AdminAuthorizeFilter]
    public class GalleryController : Controller
    {
        
        public ActionResult Index(int? day)
        {
            DBEntities db = new DBEntities();

            ViewData["day"] = day;

            return View();
        }

        [HttpPost]
        public JsonResult UpdateStatus(long id, bool value)
        {
            DBEntities db = new DBEntities();

            Photo entry = db.Photo.Find(id);
            if (entry != null)
            {
                entry.Status = value;
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new { value = entry.Status });
        }

        public JsonResult Data(jQueryDataTableParamModel param, int? day)
        {
           
            DBEntities db = new DBEntities();

            IQueryable<PhotoModel> all;

            if (day.HasValue) {
                DateTime  ToDay = new DateTime(2018, 11, 1, 0, 0, 0).AddDays(day.Value);
                DateTime EndDay = ToDay.AddDays(1).AddMilliseconds(-1);

                all = (from q in db.Photo
                        join a in db.Account on q.AccountID equals a.AccountID
                        select new PhotoModel
                        {
                            PhotoID = q.PhotoID,
                            Caption = q.Caption,
                            FileName = q.FileName,
                            CreateDate = q.CreateDate,
                            Status = q.Status,
                            Vote = db.Vote.Where(v =>
                                                    v.PhotoID == q.PhotoID &&
                                                    v.CreateDate >= ToDay &&
                                                    v.CreateDate <= EndDay
                                                ).Count(),
                            Account = new AccountModel
                            {
                                AccountID = a.AccountID,
                                Name = a.Name
                            }
                        });
            } 
            else
            {
                all = (from q in db.Photo
                       join a in db.Account on q.AccountID equals a.AccountID
                       select new PhotoModel
                       {
                           PhotoID = q.PhotoID,
                           Caption = q.Caption,
                           FileName = q.FileName,
                           CreateDate = q.CreateDate,
                           Status = q.Status,
                           Vote = db.Vote.Where(v => v.PhotoID == q.PhotoID).Count(),
                           Account = new AccountModel
                           {
                               AccountID = a.AccountID,
                               Name = a.Name
                           }
                       });
            }
            

            // filter

            IEnumerable<PhotoModel> filtered;
            if (!string.IsNullOrEmpty(param.sSearch))
            {
                filtered = all
                         .Where(a => a.Caption.Contains(param.sSearch) || a.Account.Name.Contains(param.sSearch));
            }
            else
            {
                filtered = all;
            }

            // sorting col

            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            Func<PhotoModel, dynamic> orderingFunction = (a => a.PhotoID.ToString());

            switch (sortColumnIndex)
            {
                case 0: orderingFunction = (a => a.PhotoID); break;
                case 1: orderingFunction = (a => a.FileName); break;
                case 2: orderingFunction = (a => a.Account.Name); break;
                case 3: orderingFunction = (a => a.Caption); break;
                case 4: orderingFunction = (a => a.Vote); break;
                case 5: orderingFunction = (a => a.Status); break;
                case 6: orderingFunction = (a => a.CreateDate); break;
            }

            // sorting dir

            var sortDirection = Request["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filtered = filtered.OrderBy(orderingFunction);
            else
                filtered = filtered.OrderByDescending(orderingFunction);

            // paging

            var displayed = filtered
                            .Skip(param.iDisplayStart)
                            .Take(param.iDisplayLength);

            // result

            var result = displayed.ToList().Select(a => new[]{
                a.PhotoID.ToString(),
                a.FileName,
                a.Account.Name,
                a.Caption,
                a.Vote.ToString(),
                a.Status.ToString(),
                a.CreateDate.ToString(),
                ""
            });

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = all.Count(),
                iTotalDisplayRecords = filtered.Count(),
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Export(int? day)
        {
            DBEntities db = new DBEntities();

            IEnumerable<PhotoModel> qry;

            if (day.HasValue)
            {
                DateTime ToDay = new DateTime(2018, 11, 1, 0, 0, 0).AddDays(day.Value);
                DateTime EndDay = ToDay.AddDays(1).AddMilliseconds(-1);

                qry = (from q in db.Photo
                       join a in db.Account on q.AccountID equals a.AccountID
                       select new PhotoModel
                       {
                           PhotoID = q.PhotoID,
                           Caption = q.Caption,
                           FileName = q.FileName,
                           CreateDate = q.CreateDate,
                           Status = q.Status,
                           Vote = db.Vote.Where(v =>
                                                   v.PhotoID == q.PhotoID &&
                                                   v.CreateDate >= ToDay &&
                                                   v.CreateDate <= EndDay
                                               ).Count(),
                           Account = new AccountModel
                           {
                               AccountID = a.AccountID,
                               Name = a.Name
                           }
                       });
            }
            else
            {
                qry = (from q in db.Photo
                       join a in db.Account on q.AccountID equals a.AccountID
                       select new PhotoModel
                       {
                           PhotoID = q.PhotoID,
                           Caption = q.Caption,
                           FileName = q.FileName,
                           CreateDate = q.CreateDate,
                           Status = q.Status,
                           Vote = db.Vote.Where(v => v.PhotoID == q.PhotoID).Count(),
                           Account = new AccountModel
                           {
                               AccountID = a.AccountID,
                               Name = a.Name
                           }
                       });
            }

            var data = qry.ToList();
            var dataTable = new System.Data.DataTable("Photo");
            dataTable.Columns.Add("PhotoID", typeof(int));
            dataTable.Columns.Add("URL", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Caption", typeof(string));
            dataTable.Columns.Add("Vote", typeof(int));
            dataTable.Columns.Add("Status", typeof(string));
            dataTable.Columns.Add("CreateDate", typeof(DateTime));

            foreach (var item in data)
            {
                dataTable.Rows.Add(
                  item.PhotoID,
                  "https:" + WebConfig.BaseURL + "upload/gallery/" + item.FileName,
                  item.Account.Name,
                  item.Caption,
                  item.Vote,
                  item.Status.ToString(),
                  item.CreateDate
                );
            }

            var grid = new GridView();
            grid.ShowHeaderWhenEmpty = true;
            grid.DataSource = dataTable;
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Photo.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "UTF-8";
            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return View("MyView");
        }
    }
}
