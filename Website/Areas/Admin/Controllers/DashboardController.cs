﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Website.Models;
using Website.Areas.Admin.Filters;

namespace Website.Areas.Admin.Controllers
{
    [AdminAuthorizeFilter]
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
