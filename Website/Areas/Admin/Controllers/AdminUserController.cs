﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website;

using Website.Models;
using Website.Areas.Admin.Filters;

namespace Website.Areas.Admin.Controllers
{
   
    public class AdminUserController : Controller
    {
        [AdminAuthorizeFilter]
        public ActionResult Index()
        {
            return View();
        }

        [AdminAuthorizeFilter]
        public ActionResult Create()
        {
            return View("Edit", new AdminUser());
        }

        [AdminAuthorizeFilter]
        public ActionResult Edit(int? id)
        {
            return View(new DBEntities().AdminUser.Find(id));
        }

        [HttpPost, AdminAuthorizeFilter]
        public ActionResult Update(AdminUser entry)
        {
            try
            {
                DBEntities db = new DBEntities();
                AdminUser _entry = new AdminUser();

                if (entry.AdminUserID > 0) // Edit
                {
                    // if (!new AdminInPermissionService().IsCreateList(WebContext.AdminUserID.Value)) return RedirectToAction("index");

                    _entry = db.AdminUser.Find(entry.AdminUserID);
                    if (_entry == null) return RedirectToAction("index");
                }
                else // Create
                {
                    // if (!new AdminInPermissionService().IsEditList(WebContext.AdminUserID.Value)) return RedirectToAction("index");

                    string username = entry.UserName.Trim();

                    if (db.AdminUser.Where(q => q.UserName == username) == null)
                    {
                        ViewBag.Error = "User name invalid.";
                        return View("Create", entry);
                    }

                    _entry.UserName = username;
                    _entry.Name = entry.Name;
                    _entry.Password = Encryptor.MD5Hash(entry.Password);
                    _entry.CreateDate = DateTime.Now;
                    _entry.CreateBy = WebContext.AdminUserID.Value;
                }

                // field data
                _entry.Email = entry.Email;
                _entry.Level = entry.Level;
                _entry.UpdateDate = DateTime.Now;
                _entry.UpdateBy = WebContext.AdminUserID.Value;

                if (entry.AdminUserID  == 0)
                    db.AdminUser.Add(_entry);
                else
                    db.Entry(_entry).State = EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Data invalid." + ex.Message;
                return View("Edit", entry);
            }
        }

        [AdminAuthorizeFilter]
        public ActionResult ChangePassword(long id)
        {
            return View(id);
        }

        [HttpPost, AdminAuthorizeFilter]
        public ActionResult ChangePassword(long id, string password)
        {
            try
            {
                if (WebContext.AdminUserLevel != 1) return RedirectToAction("index");

                DBEntities db = new DBEntities();
                AdminUser _entry = db.AdminUser.Find(id);

                if (_entry == null) return RedirectToAction("index");

                _entry.Password = Encryptor.MD5Hash(password);
                _entry.UpdateDate = DateTime.Now;
                _entry.UpdateBy = WebContext.AdminUserID.Value;

                db.Entry(_entry).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Data invalid." + ex.Message;
                return View("Edit", id);
            }
        }


        public ActionResult Login()
        {
            Login login = new Login();
            if (!string.IsNullOrEmpty(Request["next"])) login.Next = Request["next"];

            return View(login);
        }

        public ActionResult Logout()
        {
            WebContext.AdminUserID = null;
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {
            DBEntities db = new DBEntities();

            if (string.IsNullOrEmpty(login.UserName) || string.IsNullOrEmpty(login.Password))
            {
                login.Status = 1;
                login.Message = "*Username or password wrong";
                return View(login);
            }

            AdminUser user = db.AdminUser.Where(q => q.UserName == login.UserName).FirstOrDefault();
            if (user == null || user.Password != Encryptor.MD5Hash(login.Password))
            {
                login.Status = 2;
                login.Message = "*Username or password wrong";
                return View(login);
            }

            WebContext.AdminUserID = user.AdminUserID;
            WebContext.AdminUserLevel = user.Level;

            if (!string.IsNullOrEmpty(login.Next)) return Redirect(HttpUtility.UrlDecode(login.Next));

            return RedirectToAction("Index", "Dashboard");
        }

        [AdminAuthorizeFilter]
        public JsonResult Data()
        {
            var result = new DBEntities().AdminUser.ToList().Select(a => new[]{
                a.AdminUserID.ToString(),
                a.UserName,
                a.Level.ToString(),
                ""
            });

            return Json(new
            {
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AdminAuthorizeFilter]
        public JsonResult Delete(long id)
        {
            DBEntities db = new DBEntities();
            //if (!new AdminInPermissionService().IsDeleteList(WebContext.AdminUserID.Value)) return Json(new { message = "false" });

            AdminUser entry = db.AdminUser.Find(id);
            if (entry != null)
            {
                db.Entry(entry).State = EntityState.Deleted;
                db.SaveChanges();
            }
                
            return Json(new { message = "true", result = "" });
        }

    }
}
