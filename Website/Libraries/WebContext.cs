﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website
{
    public class WebContext
    {
        public static bool IsAdmin
        {
            get
            {
                return (AdminUserID != null);
            }
        }

        public static long? AdminUserID
        {
            get
            {
                if (HttpContext.Current.Session["AdminID"] != null)
                {
                    return (long)HttpContext.Current.Session["AdminID"];
                }

                return null;
            }

            set
            {
                HttpContext.Current.Session["AdminID"] = value;
            }
        }

        public static int? AdminUserLevel
        {
            get
            {
                if (HttpContext.Current.Session["AdminLevel"] != null)
                {
                    return (int)HttpContext.Current.Session["AdminLevel"];
                }

                return null;
            }

            set
            {
                HttpContext.Current.Session["AdminLevel"] = value;
            }
        }
    }
}