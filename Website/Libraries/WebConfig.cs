﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Website
{
    public class WebConfig
    {

        public const string ConfirmDeleteMessage = "You want to delete the selected or not.";

        public static string SiteName
        {
            get
            {
                return ConfigurationManager.AppSettings["SiteName"];
            }
        }
        public static string BaseURL
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseURL"];
            }
        }
        public static string GoogleAnalyticCode
        {
            get
            {
                return ConfigurationManager.AppSettings["GoogleAnalyticCode"];
            }
        }

        public static string SiteTitle
        {
            get
            {
                return ConfigurationManager.AppSettings["SiteTitle"];
            }
        }
        //Facebook
        public static string FacebookApplicationID
        {
            get
            {
                return ConfigurationManager.AppSettings["FacebookApplicationID"];
            }
        }
        public static string FacebookSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["FacebookSecret"];
            }
        }
        public static string BaseFacebookAppURL
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseFacebookAppURL"];
            }
        }
        public static string FacebookPermission
        {
            get
            {
                return ConfigurationManager.AppSettings["FacebookPermission"];
            }
        }
    }
}