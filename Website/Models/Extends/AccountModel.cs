﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class AccountModel : Account
    {
        public List<PhotoModel> Photos { get; set; }
        public List<Vote> Votes { get; set; }
    }
}