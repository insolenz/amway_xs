﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class PhotoModel : Photo
    {
        public AccountModel Account { get; set; }
        public bool Voted { get; set; }
    }
}