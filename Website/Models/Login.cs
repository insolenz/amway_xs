﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Website;

namespace Website.Models
{
    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public string Next { get; set; }

        public static string GetUserName()
        {
            AdminUser user = new DBEntities().AdminUser.Where(q => q.AdminUserID == WebContext.AdminUserID && q.IsDelete == false && q.Status == 0).FirstOrDefault();
            if (user == null) return "Anonymous";

            return user.Name;
        }
    }
}