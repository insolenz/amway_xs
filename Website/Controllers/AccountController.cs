﻿using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Website.Models;

namespace Website.Controllers
{
    [RequiresSSL]
    public class AccountController : Controller
    {

        public JsonResult FBLogin(string token)
        {
            var facebookClient = new FacebookClient(token);
            dynamic me = facebookClient.Get("me") as JsonObject;
            //var uid = me["id"];

            DBEntities db = new DBEntities();

            long fbid = Convert.ToInt64( me.id );
            string name = me.name;

            Account acc = db.Account.Where(q => q.FBID == fbid).FirstOrDefault();

            if (acc == null)
            {
                acc = new Account
                {
                    FBID = fbid,
                    Name = name,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                db.Account.Add(acc);
                db.SaveChanges();
            }
            else
            {
                acc.UpdateDate = DateTime.Now;

                db.Entry(acc).State = System.Data.EntityState.Modified;

                db.SaveChanges();
            }

            return Json(acc);
        }

    }
}
