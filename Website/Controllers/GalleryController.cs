﻿using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website.Models;
using Website.Services;

namespace Website.Controllers
{
    [RequiresSSL]
    public class GalleryController : Controller
    {

        private DateTime ToDay
        {
            get 
            {
                return DateTime.Today;
            }
        }

        private DateTime EndDay
        {
            get
            {
                return ToDay.AddDays(1).AddMilliseconds(-1);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Images(string order, string keyword, int count, string token)
        {
            DBEntities db = new DBEntities();

            int accID = 0;

            if (!string.IsNullOrEmpty(token))
            { 
                Account acc = Services.AccountService.AccountBYToken(token);

                if (acc != null)
                    accID = acc.AccountID;
            }

            IQueryable<PhotoModel> qry = (from q in db.Photo
                                          join a in db.Account on q.AccountID equals a.AccountID
                                            where q.Status == true
                                            select new PhotoModel
                                            {
                                                PhotoID = q.PhotoID,
                                                Caption = q.Caption,
                                                FileName = q.FileName,
                                                CreateDate = q.CreateDate,
                                                Voted = accID > 0 && db.Vote.Where(v =>
                                                                        v.PhotoID == q.PhotoID &&
                                                                        v.AccountID == accID &&
                                                                        v.CreateDate >= ToDay &&
                                                                        v.CreateDate <= EndDay
                                                                    ).Count() > 0,
                                                Vote = db.Vote.Where(v => 
                                                                        v.PhotoID == q.PhotoID &&
                                                                        v.CreateDate >= ToDay && 
                                                                        v.CreateDate <= EndDay
                                                                    ).Count(),
                                                Account = new AccountModel { 
                                                    AccountID = a.AccountID,
                                                    Name = a.Name
                                                }
                                            });

            if (!string.IsNullOrEmpty(keyword))
            {
                qry = qry.Where(q => q.Caption.Contains(keyword) || q.Account.Name.Contains(keyword));
            }

            if (order == "vote")
            {
                qry = qry.OrderByDescending(q => q.PhotoID).OrderByDescending(q => q.Vote);
            }
            else
            {
                qry = qry.OrderByDescending(q => q.PhotoID);
            }


            qry = qry.Skip(count).Take(9);

            List<PhotoModel> photos = qry.ToList();

            return Json(photos);
        }

        public JsonResult Image(int id, string token)
        {
            DBEntities db = new DBEntities();

            int accID = 0;

            if (!string.IsNullOrEmpty(token))
            {
                Account acc = Services.AccountService.AccountBYToken(token);

                if (acc != null)
                    accID = acc.AccountID;
            }

            IQueryable<PhotoModel> qry = (from q in db.Photo
                                          join a in db.Account on q.AccountID equals a.AccountID
                                          where q.Status == true
                                          select new PhotoModel
                                          {
                                              PhotoID = q.PhotoID,
                                              Caption = q.Caption,
                                              FileName = q.FileName,
                                              CreateDate = q.CreateDate,
                                              Voted = accID > 0 && db.Vote.Where(v =>
                                                                      v.PhotoID == q.PhotoID &&
                                                                      v.AccountID == accID &&
                                                                      v.CreateDate >= ToDay &&
                                                                      v.CreateDate <= EndDay
                                                                  ).Count() > 0,
                                              Vote = db.Vote.Where(v =>
                                                                      v.PhotoID == q.PhotoID &&
                                                                      v.CreateDate >= ToDay &&
                                                                      v.CreateDate <= EndDay
                                                                  ).Count(),
                                              Account = new AccountModel
                                              {
                                                  AccountID = a.AccountID,
                                                  Name = a.Name
                                              }
                                          });

            return Json(qry.Where(q => q.PhotoID == id).FirstOrDefault());
        }


        public JsonResult UploadImage(string token, string image, string shareImage, string caption)
        {
            Account acc = AccountService.AccountBYToken(token);

            if (acc == null)
                return Json(new { error = "permission denied" });

            string imagePath = "~/Upload/Gallery/";
            string imageFileName = Guid.NewGuid() + ".png";

            System.IO.File.WriteAllBytes(Server.MapPath(imagePath + imageFileName), Convert.FromBase64String(image.Replace("data:image/png;base64,", "")));

            System.IO.File.WriteAllBytes(Server.MapPath(imagePath + "share_" + imageFileName), Convert.FromBase64String(shareImage.Replace("data:image/png;base64,", "")));

            Photo photo = new Photo {
                AccountID = acc.AccountID,
                FileName = imageFileName,
                Caption = caption,
                Vote = 0,
                Share = 0,
                Status = true,
                CreateDate = DateTime.Now
            };

            DBEntities db = new DBEntities();

            db.Photo.Add(photo);
            db.SaveChanges();

            var facebookClient = new FacebookClient(token);

            var postParams = new
            {
                scrape = true,
                id = "https:" + WebConfig.BaseURL + "gallery/photo/" + photo.PhotoID
            };
           
            facebookClient.Post(postParams);

            photo.FileName = "https:" + WebConfig.BaseURL + "gallery/photo/" + photo.PhotoID;

            return Json(photo);
        }

        public JsonResult Vote(string token, int id) 
        {
            Account acc = AccountService.AccountBYToken(token);

            if (acc == null)
                return Json(new { error = "permission denied" });

            DBEntities db = new DBEntities();

            Photo photo = db.Photo.Find(id);

            if (photo == null)
                return Json(new { error = "data invalid" });

            int voteCount = db.Vote.Where(q => 
                q.PhotoID == photo.PhotoID && 
                q.AccountID == acc.AccountID &&
                q.CreateDate >= ToDay && 
                q.CreateDate <= EndDay
            ).Count();

            if (voteCount == 0)
            {
                Vote vote = new Vote
                {
                    PhotoID = photo.PhotoID,
                    AccountID = acc.AccountID,
                    CreateDate = DateTime.Now
                };

                db.Vote.Add(vote);
                db.SaveChanges();

                // daily
                photo.Vote = db.Vote.Where(q => 
                    q.PhotoID == photo.PhotoID &&
                    q.CreateDate >= ToDay && 
                    q.CreateDate <= EndDay
                ).Count();

                db.Entry(photo).State = System.Data.EntityState.Modified;

                db.SaveChanges();

                return Json(new { count = photo.Vote });
            }
            else
            {
                return Json(new { error = "voted" });
            }
            
        }

        public ActionResult Photo(int id)
        {
            DBEntities db = new DBEntities();

            Photo photo = db.Photo.Find(id);

            if (photo == null)
                return RedirectToAction("Index", "Home");

            Account account = db.Account.Find(photo.AccountID);

            if (account == null)
                return RedirectToAction("Index", "Home");

            Photo p = new PhotoModel
            {
                PhotoID = photo.PhotoID,
                FileName = photo.FileName,
                Account = new AccountModel
                {
                    Name = account.Name
                }
            };

            return View(p);

        }
    
    }
}