﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Website
{
    public class InternationalizationAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (HttpContext.Current.Request.Url.LocalPath.ToLower().IndexOf("/home") == -1)
            {
                if (new FileInfo(HttpContext.Current.Server.MapPath("~/index.html")).Exists)
                {
                    HttpContext.Current.Response.Redirect("~/index.html");
                }
            }

        }
    }
}